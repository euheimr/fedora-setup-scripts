#!/bin/bash
sudo dnf update -y
sudo dnf upgrade -y
sudo dnf install ufw -y

# Enable UFW
# See 'man ufw'
sudo ufw enable

# Disable FirewallD
sudo systemctl disable --now firewalld.service

# Print network stats
sudo netstat -lpn --inet
sudo netstat -lpn --inet6

# Default to block every incoming and outgoing connection except those specifically allow-listed by other firewall rules
sudo ufw default deny incoming
sudo ufw default deny outgoing

# Allows six incoming connections to the SSH service on port 22 from the same source IP per 30-second interval
sudo ufw limit in 22/tcp comment "rate-limit SSH"

# Allows VNC connections from the private 10.0.0.0/8 IP subnet
# sudo ufw allow proto tcp from 10.0.0.0/8 to any port 5900

# Enable UFW
sudo systemctl enable --now ufw.service
sudo ufw status
