#!/bin/bash

yum install sudo
sudo dnf update -y 
sudo dnf install git
sudo yum install java-1.8.0-openjdk perl perl-JSON perl-libwww-perl perl-LWP-Protocol-https python make wget git rdiff-backup rsync socat iptables sudo procps which

sudo git clone https://github.com/MinecraftServerControl/mscs.git && cd mscs
sudo make install
