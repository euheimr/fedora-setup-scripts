#!/bin/bash
sudo dnf update -y
sudo dnf upgrade -y

# Print network stats
#sudo netstat -lpn --inet
#sudo netstat -lpn --inet6

##  GROUPS  ##
##  sudo dnf groups list

#   Development Groups  #
sudo dnf groups install "Development Tools" "Headless Management" "LibreOffice" "Python Science" "Security Lab" "System Tools" -y
sudo dnf groups install "C Development Tools and Libraries" -y
sudo dnf groups install "D Development Tools and Libraries" -y

#   Cloud tools Groups  #
sudo dnf groups install "Cloud Infrastructure" "Cloud Management Tools" "Container Management" -y

#   Docker    #
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io

##  Main sources ##
# Packages to install
sudo dnf install -y ufw cryptsetup openvpn network-manager-openvpn

# Python and other misc
sudo dnf install python make wget git rdiff-backup rsync socat iptables procps rsync wheel which -y

# Bitwarden (pwd mgr)
echo "Bitwarden GO HERE -> https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=rpm"
#sudo dnf install -y ''

##  WEB BROWSERS ##
# Brave Web Browser
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf install brave-browser

#   TOR 
# wget https://www.torproject.org/dist/torbrowser/9.0.10/tor-browser-linux64-9.0.10_en-US.tar.xz
# mv /home/jakal/Downloads/tor-browser_en-US/Tor-Browser/ /opt/Tor-Browser

# make TOR desktop shortcut
# [Desktop Entry]
# Type=Application
# Name=Tor Browser
# GenericName=Web Browser
# Comment=Tor Browser is +1 for privacy and −1 for mass surveillance
# Categories=Network;WebBrowser;Security;
# Exec=sh -c '"/opt/Tor-Browser/start-tor-browser" --detach || ([ !  -x "/opt/Tor-Browser/start-tor-browser" ] && "$(dirname "$*")"/Browser/start-tor-browser --detach)' dummy %k
# X-TorBrowser-ExecShell=./Browser/start-tor-browser --detach
# Icon=/opt/Tor-Browser/browser/chrome/icons/default/default128.png
# StartupWMClass=Tor Browser

# mv /home/jakal/Desktop/start-tor-browser.desktop /usr/share/applications/


# GitKraken GIT Client
sudo dnf install -y 'https://release.gitkraken.com/linux/gitkraken-amd64.rpm'

# VS Code
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf check-update
sudo dnf install code

##  SDKs    ##

# dotnet SDK 3.1
sudo dnf install dotnet-sdk-3.1 -y
sudo dnf install dotnet-runtime-3.1 -y

# Perl
sudo dnf install perl perl-JSON perl-libwww-perl perl-LWP-Protocol-https -y

## DATABASES ##

# PostgreSQL: Fedora 32
sudo dnf install https://download.postgresql.org/pub/repos/yum/reporpms/F-32-x86_64/pgdg-fedora-repo-latest.noarch.rpm -y
sudo dnf install postgresql12 postgresql12-server postgresql12-contrib pgadmin4 -y
sudo /usr/pgsql-12/bin/postgresql-12-setup initdb
# Enable PGSQL
sudo systemctl enable postgresql-12
sudo systemctl start postgresql-12

#   Eclipse Group   #
#sudo dnf groups install "Fedora Eclipse"

#   Creative tools Groups   #
#sudo dnf groups install "Audio Production" "Sound and Video" "Authoring and Publishing" "Books and Guides" "Design Suite" "Office/Productivity"

#   Minecraft Sources   #
# yum install sudo
# sudo dnf update -y 
# sudo dnf install git
# sudo yum install java-1.8.0-openjdk perl perl-JSON perl-libwww-perl perl-LWP-Protocol-https python make wget git rdiff-backup rsync socat iptables sudo procps which
# sudo git clone https://github.com/MinecraftServerControl/mscs.git && cd mscs
# sudo make install


# Clean-up tasks
sudo dnf update -y
sudo dnf autoremove -y
sudo dnf clean all